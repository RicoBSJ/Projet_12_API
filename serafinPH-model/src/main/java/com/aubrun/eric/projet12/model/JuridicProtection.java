package com.aubrun.eric.projet12.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "juridic_protection_id_generator", sequenceName = "juridic_protection_id_seq", allocationSize = 1)
@Table(name = "JURIDIC_PROTECTION")
@Proxy(lazy = false)
public class JuridicProtection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "juridic_protection_id_generator")
    @Column(name = "juridic_protection_id")
    private Integer juridicProtectionId;
    @Enumerated(EnumType.STRING)
    @Column(name = "juridic_protection_name")
    private EJuridicProtection juridicProtectionName;

    public JuridicProtection() {
    }

    public Integer getJuridicProtectionId() {
        return juridicProtectionId;
    }

    public void setJuridicProtectionId(Integer juridicProtectionId) {
        this.juridicProtectionId = juridicProtectionId;
    }

    public EJuridicProtection getJuridicProtectionName() {
        return juridicProtectionName;
    }

    public void setJuridicProtectionName(EJuridicProtection juridicProtectionName) {
        this.juridicProtectionName = juridicProtectionName;
    }

    @Override
    public String toString() {
        return "JuridicProtection{" +
                "juridicProtectionId=" + juridicProtectionId +
                ", juridicProtectionName=" + juridicProtectionName +
                '}';
    }
}
