package com.aubrun.eric.projet12.model;

public enum EEstablishment {

    FOYER_HEB,
    FOYER_VIE,
    OTHER
}
