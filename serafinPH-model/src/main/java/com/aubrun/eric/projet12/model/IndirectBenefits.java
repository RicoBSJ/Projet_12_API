package com.aubrun.eric.projet12.model;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "indirect_benefits_id_generator", sequenceName = "indirect_benefits_id_seq", allocationSize = 1)
@Table(name = "INDIRECT_BENEFITS")
public class IndirectBenefits {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "indirect_benefits_id_generator")
    @Column(name = "indirect_benefits_id")
    private Integer indirectBenefitsId;
    @Column(name = "indirect_benefit_name")
    private String indirectBenefitName;
    @Enumerated(EnumType.STRING)
    @Column(name = "indirect_benefit_kind")
    private EIndirectBenefits indirectBenefitType;

    public IndirectBenefits() {
    }

    public Integer getIndirectBenefitsId() {
        return indirectBenefitsId;
    }

    public void setIndirectBenefitsId(Integer indirectBenefitsId) {
        this.indirectBenefitsId = indirectBenefitsId;
    }

    public String getIndirectBenefitName() {
        return indirectBenefitName;
    }

    public void setIndirectBenefitName(String indirectBenefitName) {
        this.indirectBenefitName = indirectBenefitName;
    }

    public EIndirectBenefits getIndirectBenefitType() {
        return indirectBenefitType;
    }

    public void setIndirectBenefitType(EIndirectBenefits indirectBenefitType) {
        this.indirectBenefitType = indirectBenefitType;
    }

    @Override
    public String toString() {
        return "IndirectBenefits{" +
                "indirectBenefitsId=" + indirectBenefitsId +
                ", indirectBenefitName='" + indirectBenefitName + '\'' +
                ", indirectBenefitType=" + indirectBenefitType +
                '}';
    }
}
