package com.aubrun.eric.projet12.model.payload.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

public class SignupRequest {

    @NotNull
    @Size(min = 3, max = 20)
    private String lastName;

    @NotNull
    @Size(min = 3, max = 20)
    private String firstName;

    @NotNull
    @Size(max = 50)
    private Integer phoneUser;

    @NotNull
    @Size(max = 50)
    private String email;

    @NotNull
    @Size(max = 50)
    private Long socialSecurityNumberUser;

    @NotNull
    private LocalDate dateBirthUser;

    @NotNull
    private LocalDateTime entryDateUser;

    @NotNull
    @Size(max = 2)
    private Integer ageUser;

    private Set<String> role;

    @NotNull
    @Size(min = 6, max = 40)
    private String password;
    /*
    @NotNull
    private String username;
     */

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getPhoneUser() {
        return phoneUser;
    }

    public void setPhoneUser(Integer phoneUser) {
        this.phoneUser = phoneUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getSocialSecurityNumberUser() {
        return socialSecurityNumberUser;
    }

    public void setSocialSecurityNumberUser(Long socialSecurityNumberUser) {
        this.socialSecurityNumberUser = socialSecurityNumberUser;
    }

    public LocalDate getDateBirthUser() {
        return dateBirthUser;
    }

    public void setDateBirthUser(LocalDate dateBirthUser) {
        this.dateBirthUser = dateBirthUser;
    }

    public LocalDateTime getEntryDateUser() {
        return entryDateUser;
    }

    public void setEntryDateUser(LocalDateTime entryDateUser) {
        this.entryDateUser = entryDateUser;
    }

    public Integer getAgeUser() {
        return ageUser;
    }

    public void setAgeUser(Integer ageUser) {
        this.ageUser = ageUser;
    }

    public Set<String> getRole() {
        return role;
    }

    public void setRole(Set<String> role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}