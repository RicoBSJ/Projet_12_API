package com.aubrun.eric.projet12.model;

import javax.persistence.*;

@Entity
@SequenceGenerator(name="needs_id_generator", sequenceName = "needs_id_seq", allocationSize=1)
@Table(name = "NEEDS")
public class Needs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "needs_id_generator")
    @Column(name = "needs_id")
    private Integer needsId;
    @Column(name = "need_name")
    private String needName;
    @Enumerated(EnumType.STRING)
    @Column(name = "need_kind")
    private ENeeds needType;

    public Needs() {
    }

    public Integer getNeedsId() {
        return needsId;
    }

    public void setNeedsId(Integer needsId) {
        this.needsId = needsId;
    }

    public String getNeedName() {
        return needName;
    }

    public void setNeedName(String needName) {
        this.needName = needName;
    }

    public ENeeds getNeedType() {
        return needType;
    }

    public void setNeedType(ENeeds needType) {
        this.needType = needType;
    }

    @Override
    public String toString() {
        return "Needs{" +
                "needsId=" + needsId +
                ", needName='" + needName + '\'' +
                ", needType=" + needType +
                '}';
    }
}
