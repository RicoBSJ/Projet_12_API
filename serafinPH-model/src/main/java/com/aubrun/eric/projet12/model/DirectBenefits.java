package com.aubrun.eric.projet12.model;

import javax.persistence.*;

@Entity
@SequenceGenerator(name="direct_benefits_id_generator", sequenceName = "direct_benefits_id_seq", allocationSize=1)
@Table(name = "DIRECT_BENEFITS")
public class DirectBenefits {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "direct_benefits_id_generator")
    @Column(name = "direct_benefits_id")
    private Integer directBenefitsId;
    @Column(name = "direct_benefit_name")
    private String directBenefitName;
    @Enumerated(EnumType.STRING)
    @Column(name = "direct_benefit_kind")
    private EDirectBenefits directBenefitType;

    public DirectBenefits() {
    }

    public Integer getDirectBenefitsId() {
        return directBenefitsId;
    }

    public void setDirectBenefitsId(Integer directBenefitsId) {
        this.directBenefitsId = directBenefitsId;
    }

    public String getDirectBenefitName() {
        return directBenefitName;
    }

    public void setDirectBenefitName(String directBenefitName) {
        this.directBenefitName = directBenefitName;
    }

    public EDirectBenefits getDirectBenefitType() {
        return directBenefitType;
    }

    public void setDirectBenefitType(EDirectBenefits directBenefitType) {
        this.directBenefitType = directBenefitType;
    }

    @Override
    public String toString() {
        return "DirectBenefits{" +
                "directBenefitsId=" + directBenefitsId +
                ", directBenefitName='" + directBenefitName + '\'' +
                ", directBenefitType=" + directBenefitType +
                '}';
    }
}
