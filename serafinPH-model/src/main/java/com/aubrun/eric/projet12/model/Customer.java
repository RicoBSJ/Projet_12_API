package com.aubrun.eric.projet12.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@SequenceGenerator(name = "customer_id_generator", sequenceName = "customer_id_seq", allocationSize = 1)
@Table(name = "CUSTOMER")
@Proxy(lazy = false)
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "customer_id_generator")
    @Column(name = "customer_id")
    private Integer customerId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nomenclature_user")
    private NomenclatureUser nomenclatureUser;
    @Column(name = "customer_first_name")
    private String customerFirstName;
    @Column(name = "customer_last_name")
    private String customerLastName;
    @Column(name = "date_birth")
    private LocalDate dateBirth;
    @Column(name = "social_security_number")
    private Long socialSecurityNumber;
    @Column(name = "mutual_name")
    private String mutualName;
    @Column(name = "entry_date")
    private LocalDateTime entryDate;
    @Column(name = "release_date")
    private LocalDateTime releaseDate;
    @Column(name = "age_customer")
    private Integer age;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "juridic_protection_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "juridic_protection_id"))
    private Set<JuridicProtection> juridicProtectionList;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "establishment_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "establishment_id"))
    private Set<Establishment> establishmentList;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "needs_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "needs_id"))
    private List<Needs> needsList;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "direct_benefits_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "direct_benefits_id"))
    private List<DirectBenefits> directBenefitsList;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "indirect_benefits_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "indirect_benefits_id"))
    private List<IndirectBenefits> indirectBenefitsList;

    public Customer() {
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public NomenclatureUser getNomenclatureUser() {
        return nomenclatureUser;
    }

    public void setNomenclatureUser(NomenclatureUser nomenclatureUser) {
        this.nomenclatureUser = nomenclatureUser;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public LocalDate getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(LocalDate dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(Long socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getMutualName() {
        return mutualName;
    }

    public void setMutualName(String mutualName) {
        this.mutualName = mutualName;
    }

    public LocalDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Set<JuridicProtection> getJuridicProtectionList() {
        return juridicProtectionList;
    }

    public void setJuridicProtectionList(Set<JuridicProtection> juridicProtectionList) {
        this.juridicProtectionList = juridicProtectionList;
    }

    public Set<Establishment> getEstablishmentList() {
        return establishmentList;
    }

    public void setEstablishmentList(Set<Establishment> establishmentList) {
        this.establishmentList = establishmentList;
    }

    public List<Needs> getNeedsList() {
        return needsList;
    }

    public void setNeedsList(List<Needs> needsList) {
        this.needsList = needsList;
    }

    public List<DirectBenefits> getDirectBenefitsList() {
        return directBenefitsList;
    }

    public void setDirectBenefitsList(List<DirectBenefits> directBenefitsList) {
        this.directBenefitsList = directBenefitsList;
    }

    public List<IndirectBenefits> getIndirectBenefitsList() {
        return indirectBenefitsList;
    }

    public void setIndirectBenefitsList(List<IndirectBenefits> indirectBenefitsList) {
        this.indirectBenefitsList = indirectBenefitsList;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", nomenclatureUser=" + nomenclatureUser +
                ", customerFirstName='" + customerFirstName + '\'' +
                ", customerLastName='" + customerLastName + '\'' +
                ", dateBirth=" + dateBirth +
                ", socialSecurityNumber=" + socialSecurityNumber +
                ", mutualName='" + mutualName + '\'' +
                ", entryDate=" + entryDate +
                ", releaseDate=" + releaseDate +
                ", age=" + age +
                ", juridicProtectionList=" + juridicProtectionList +
                ", establishmentList=" + establishmentList +
                ", needsList=" + needsList +
                ", directBenefitsList=" + directBenefitsList +
                ", indirectBenefitsList=" + indirectBenefitsList +
                '}';
    }
}
