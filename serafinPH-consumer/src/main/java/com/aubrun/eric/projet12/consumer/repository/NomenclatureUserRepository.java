package com.aubrun.eric.projet12.consumer.repository;

import com.aubrun.eric.projet12.model.NomenclatureUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NomenclatureUserRepository extends JpaRepository<NomenclatureUser, Integer> {

    Optional<NomenclatureUser> findNomenclatureUserByEmail(String email);
    Optional<NomenclatureUser> findNomenclatureUserByLastName(String lastName);
    boolean existsByEmail (String email);

    @Modifying
    @Query(value = "DELETE FROM user_establishments WHERE user_establishment_id=:nomenclatureUserId",nativeQuery = true)
    void delEstablishment(Integer nomenclatureUserId);

    @Modifying
    @Query(value = "DELETE FROM user_roles WHERE user_roles_id=:nomenclatureUserId",nativeQuery = true)
    void delRole(Integer nomenclatureUserId);
}
