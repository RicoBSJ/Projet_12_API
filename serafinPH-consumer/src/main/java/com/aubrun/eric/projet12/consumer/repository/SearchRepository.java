package com.aubrun.eric.projet12.consumer.repository;

import com.aubrun.eric.projet12.model.Customer;
import com.aubrun.eric.projet12.model.SearchCustomer;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SearchRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Customer> findAllByNameAndDateBirthAndEntryDateAndAge(SearchCustomer searchCustomer) {
        Session session = entityManager.unwrap(Session.class);
        List<Customer> resultat;
        Map<String, Object> parameters = new HashMap<>();
        String q = "SELECT c FROM Customer c WHERE 1=1";
        if (searchCustomer.getNomenclatureUser() != null && !searchCustomer.getNomenclatureUser().equals("")) {
            q += " AND UPPER(c.nomenclatureUser.lastName) LIKE :lastName";
            parameters.put("lastName", "%" + searchCustomer.getNomenclatureUser().toUpperCase() + "%");
        }
        if (searchCustomer.getCustomerFirstName() != null && !searchCustomer.getCustomerFirstName().equals("")) {
            q += " AND UPPER(c.customerFirstName) LIKE :customerFirstName";
            parameters.put("customerFirstName", "%" + searchCustomer.getCustomerFirstName().toUpperCase() + "%");
        }
        if (searchCustomer.getCustomerLastName() != null && !searchCustomer.getCustomerLastName().equals("")) {
            q += " AND UPPER(c.customerLastName) LIKE :customerLastName";
            parameters.put("customerLastName", "%" + searchCustomer.getCustomerLastName().toUpperCase() + "%");
        }
        if (searchCustomer.getSocialSecurityNumber() != null){
            q += " AND c.socialSecurityNumber LIKE :socialSecurityNumber";
            parameters.put("socialSecurityNumber", "%" + searchCustomer.getSocialSecurityNumber());
        }
        if (searchCustomer.getMutualName() != null){
            q += " AND c.mutualName LIKE :mutualName";
            parameters.put("mutualName", searchCustomer.getMutualName());
        }
        if (searchCustomer.getDateBirth() != null) {
            q += " AND c.dateBirth LIKE :dateBirth";
            parameters.put("dateBirth", searchCustomer.getDateBirth());
        }
        if (searchCustomer.getEntryDate() != null) {
            q += " AND c.entryDate LIKE :entryDate";
            parameters.put("entryDate", searchCustomer.getEntryDate());
        }
        if (searchCustomer.getAge() != null) {
            q += " AND c.age LIKE :age";
            parameters.put("age", searchCustomer.getAge());
        }
        Query<Customer> query = session.createQuery(q);
        query.setProperties(parameters);
        resultat = query.getResultList();
        return resultat;
    }
}
