package com.aubrun.eric.projet12.consumer.repository;

import com.aubrun.eric.projet12.model.Customer;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    @Modifying
    @Query(value = "DELETE FROM juridic_protection_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByJuridicProtectionList(Integer customerId);

    @Modifying
    @Query(value = "DELETE FROM establishment_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByEstablishmentList(Integer customerId);

    @Modifying
    @Query(value = "DELETE FROM needs_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByNeedsList(Integer customerId);

    @Modifying
    @Query(value = "DELETE FROM direct_benefits_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByDirectBenefits(Integer customerId);

    @Modifying
    @Query(value = "DELETE FROM indirect_benefits_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByIndirectBenefits(Integer customerId);

    @Query(value = "SELECT c FROM Customer c WHERE c.nomenclatureUser=:nomenclatureUser")
    List<Customer> findAllByNomenclatureUser (NomenclatureUser nomenclatureUser);
}
