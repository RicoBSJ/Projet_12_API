package com.aubrun.eric.projet12.consumer.repository;

import com.aubrun.eric.projet12.model.Needs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NeedsRepository extends JpaRepository<Needs, Integer> {

}
