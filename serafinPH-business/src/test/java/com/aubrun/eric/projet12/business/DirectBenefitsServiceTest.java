package com.aubrun.eric.projet12.business;

import com.aubrun.eric.projet12.business.mapper.DirectBenefitsDtoMapper;
import com.aubrun.eric.projet12.business.service.DirectBenefitsService;
import com.aubrun.eric.projet12.consumer.repository.*;
import com.aubrun.eric.projet12.model.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class DirectBenefitsServiceTest {

    @Autowired
    private DirectBenefitsService directBenefitsService;
    @MockBean
    private CustomerRepository customerRepository;
    @MockBean
    private NomenclatureUserRepository nomenclatureUserRepository;
    @MockBean
    private EstablishmentRepository establishmentRepository;
    @MockBean
    private JuridicProtectionRepository juridicProtectionRepository;
    @MockBean
    private NeedsRepository needsRepository;
    @MockBean
    private DirectBenefitsRepository directBenefitsRepository;
    @MockBean
    private IndirectBenefitsRepository indirectBenefitsRepository;
    @MockBean
    private RoleRepository roleRepository;
    @MockBean
    private SearchRepository searchRepository;

    static private Principal principal;
    static private NomenclatureUser nomenclatureUser;
    static private Customer customer;
    static private SearchCustomer searchCustomer;
    static private JuridicProtection juridicProtection;
    static private Establishment establishment;
    static private Needs needs;
    static private DirectBenefits directBenefits;
    static private IndirectBenefits indirectBenefits;
    static private Role role;
    static private Set<JuridicProtection> juridicProtectionSet;
    static private List<JuridicProtection> juridicProtectionList;
    static private List<Integer> integerListJuridicProtection;
    static private Set<Establishment> establishmentSet;
    static private List<Establishment> establishmentList;
    static private List<Integer> integerListEstablishment;
    static private List<Needs> needsList;
    static private List<DirectBenefits> directBenefitsList;
    static private List<IndirectBenefits> indirectBenefitsList;
    static private List<Customer> customerList;
    static private Set<Role> roleSet;
    static private List<Integer> needsInt;
    static private List<Integer> directBenefitsInt;
    static private List<Integer> inDirectBenefitsInt;
    static private Set<Integer> establishmentInt;
    static private Set<Integer> juridicProtectionInt;
    static private LocalDateTime releaseDate;

    @BeforeAll
    static void setUpBeforeEach(){

        needsInt = new ArrayList<>();
        needsInt.add(1);

        directBenefitsInt = new ArrayList<>();
        directBenefitsInt.add(20);

        inDirectBenefitsInt = new ArrayList<>();
        inDirectBenefitsInt.add(30);

        establishmentInt = new HashSet<>();
        establishmentInt.add(40);
        integerListEstablishment = List.copyOf(establishmentInt);

        juridicProtectionInt = new HashSet<>();
        juridicProtectionInt.add(50);
        integerListJuridicProtection = List.copyOf(juridicProtectionInt);

        principal = Mockito.mock(Principal.class);

        juridicProtection = new JuridicProtection();
        juridicProtection.setJuridicProtectionId(1);
        juridicProtection.setJuridicProtectionName(EJuridicProtection.valueOf("TRUSTEESHIP"));

        establishment = new Establishment();
        establishment.setEstablishmentId(2);
        establishment.setEstablishmentName(EEstablishment.valueOf("FOYER_HEB"));

        needs = new Needs();
        needs.setNeedsId(3);
        needs.setNeedName("1.1.1.1 Besoins en matière de fonctions mentales, psychiques, cognitives et du système nerveux");
        needs.setNeedType(ENeeds.valueOf("HEALTH_NEEDS"));

        directBenefits = new DirectBenefits();
        directBenefits.setDirectBenefitsId(10);
        directBenefits.setDirectBenefitName("2.1.1.1 Soins médicaux à visée préventive, curative et palliative");
        directBenefits.setDirectBenefitType(EDirectBenefits.valueOf("HEALTH_DIRECT_BENEFITS"));

        indirectBenefits = new IndirectBenefits();
        indirectBenefits.setIndirectBenefitsId(11);
        indirectBenefits.setIndirectBenefitName("3.1.1.1 Pilotage et direction");
        indirectBenefits.setIndirectBenefitType(EIndirectBenefits.valueOf("MANAGE_AND_COOPERATION_FUNCTION"));

        juridicProtectionSet = new HashSet<>();
        juridicProtectionSet.add(juridicProtection);
        juridicProtectionList = List.copyOf(juridicProtectionSet);

        establishmentSet = new HashSet<>();
        establishmentSet.add(establishment);
        establishmentList = List.copyOf(establishmentSet);

        needsList = new ArrayList<>();
        needsList.add(needs);

        directBenefitsList = new ArrayList<>();
        directBenefitsList.add(directBenefits);

        indirectBenefitsList = new ArrayList<>();
        indirectBenefitsList.add(indirectBenefits);

        customer = new Customer();
        customer.setCustomerId(5);
        customer.setCustomerFirstName("Bob");
        customer.setCustomerLastName("GABE");
        customer.setDateBirth(LocalDate.parse("1965-02-19"));
        customer.setSocialSecurityNumber(265021304502158L);
        customer.setMutualName("VARMUT");
        customer.setEntryDate(LocalDateTime.parse("2022-09-22T07:00"));
        customer.setAge(57);
        customer.setJuridicProtectionList(juridicProtectionSet);
        customer.setEstablishmentList(establishmentSet);
        customer.setNeedsList(needsList);
        customer.setDirectBenefitsList(directBenefitsList);
        customer.setIndirectBenefitsList(indirectBenefitsList);

        customerList = new ArrayList<>();
        customerList.add(customer);

        role = new Role();
        role.setRoleId(12);
        role.setRoleName(ERole.valueOf("ROLE_USER"));

        roleSet = new HashSet<>();
        roleSet.add(role);

        nomenclatureUser = new NomenclatureUser();
        nomenclatureUser.setNomenclatureUserId(3);
        nomenclatureUser.setFirstName("John");
        nomenclatureUser.setLastName("BRYAN");
        nomenclatureUser.setDateBirthUser(LocalDate.parse("1965-02-18"));
        nomenclatureUser.setSocialSecurityNumberUser(165021307805423L);
        nomenclatureUser.setPhoneUser(788650023);
        nomenclatureUser.setEntryDateUser(LocalDateTime.parse("2022-08-05T07:00"));
        nomenclatureUser.setAgeUser(57);
        nomenclatureUser.setPassword("My Password");
        nomenclatureUser.setEmail("john.bryan@gmail.com");
        nomenclatureUser.setCustomerList(customerList);
        nomenclatureUser.setRoles(roleSet);
        nomenclatureUser.setEstablishments(establishmentSet);

        customer.setNomenclatureUser(nomenclatureUser);

        searchCustomer = new SearchCustomer();
        searchCustomer.setCustomerId(5);
        searchCustomer.setCustomerFirstName("Bob");
        searchCustomer.setCustomerLastName("GABE");
        searchCustomer.setDateBirth(LocalDate.parse("1965-02-19"));
        searchCustomer.setSocialSecurityNumber(265021304502158L);
        searchCustomer.setMutualName("VARMUT");
        searchCustomer.setEntryDate(LocalDateTime.parse("2022-09-22T07:00"));
        searchCustomer.setAge(57);
    }

    @Test
    public void findAll() {
        directBenefitsService.findAll();
        Optional<DirectBenefits> optionalDirectBenefits = directBenefitsRepository.findById(directBenefits.getDirectBenefitsId());
        assertThat(optionalDirectBenefits).isNotNull();
    }

    @Test
    public void findById() {
        when(directBenefitsRepository.findById(directBenefits.getDirectBenefitsId())).thenReturn(Optional.ofNullable(directBenefits));
        directBenefitsService.findById(directBenefits.getDirectBenefitsId());
        Optional<DirectBenefits> directBenefitsOptional = directBenefitsRepository.findById(directBenefits.getDirectBenefitsId());
        assertThat(directBenefitsOptional).isNotNull();
    }

    @Test
    public void save() {
        when(nomenclatureUserRepository.findNomenclatureUserByLastName(nomenclatureUser.getLastName())).thenReturn(Optional.ofNullable(nomenclatureUser));
        directBenefitsService.save(nomenclatureUser.getLastName(), DirectBenefitsDtoMapper.toDto(directBenefits));
        Optional<DirectBenefits> optionalDirectBenefits = directBenefitsRepository.findById(directBenefits.getDirectBenefitsId());
        assertThat(optionalDirectBenefits).isNotNull();
    }

    @Test
    public void update() {
        when(directBenefitsRepository.findById(directBenefits.getDirectBenefitsId())).thenReturn(Optional.ofNullable(directBenefits));
        directBenefitsService.update(DirectBenefitsDtoMapper.toDto(directBenefits), directBenefits.getDirectBenefitsId());
        Optional<DirectBenefits> directBenefitsOptional = directBenefitsRepository.findById(directBenefits.getDirectBenefitsId());
        assertThat(directBenefitsOptional).isNotNull();
    }

    @Test
    public void deleteById(){
        when(directBenefitsRepository.findById(directBenefits.getDirectBenefitsId())).thenReturn(Optional.ofNullable(directBenefits));
        directBenefitsService.deleteById(directBenefits.getDirectBenefitsId());
        Optional<DirectBenefits> optionalDirectBenefits = directBenefitsRepository.findById(directBenefits.getDirectBenefitsId());
        assertThat(optionalDirectBenefits).isNotNull();
    }
}
