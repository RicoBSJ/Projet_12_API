package com.aubrun.eric.projet12.business.dto;

import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.util.Collections;
import java.util.List;

@ExcludeClassFromJacocoGeneratedReport
public class AddNeedsDto {

    private List<Integer> needs;

    public AddNeedsDto(int needs) {
        this.needs = Collections.singletonList(needs);
    }

    public List<Integer> getNeeds() {
        return needs;
    }

    public void setNeeds(List<Integer> needs) {
        this.needs = needs;
    }
}
