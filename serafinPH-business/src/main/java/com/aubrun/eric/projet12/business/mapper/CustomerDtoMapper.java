package com.aubrun.eric.projet12.business.mapper;

import com.aubrun.eric.projet12.business.dto.CustomerDto;
import com.aubrun.eric.projet12.model.Customer;
import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.util.stream.Collectors;

@ExcludeClassFromJacocoGeneratedReport
public class CustomerDtoMapper {

    static public CustomerDto toDto(Customer customer) {

        CustomerDto dto = new CustomerDto();
        dto.setCustomerId(customer.getCustomerId());
        dto.setCustomerFirstName(customer.getCustomerFirstName());
        dto.setCustomerLastName(customer.getCustomerLastName());
        dto.setNomenclatureUser(customer.getNomenclatureUser() == null ? "" :customer.getNomenclatureUser().getFirstName()+" "+customer.getNomenclatureUser().getLastName());
        dto.setAge(customer.getAge());
        dto.setDateBirth(customer.getDateBirth());
        dto.setSocialSecurityNumber(customer.getSocialSecurityNumber());
        dto.setMutualName(customer.getMutualName());
        dto.setEntryDate(customer.getEntryDate());
        dto.setReleaseDate(customer.getReleaseDate());
        dto.setJuridicProtectionList(customer.getJuridicProtectionList().stream().map(JuridicProtectionDtoMapper::toDto).collect(Collectors.toSet()));
        dto.setEstablishmentList(customer.getEstablishmentList().stream().map(EstablishmentDtoMapper::toDto).collect(Collectors.toSet()));
        dto.setNeedsList(customer.getNeedsList().stream().map(NeedsDtoMapper::toDto).collect(Collectors.toList()));
        dto.setDirectBenefitsList(customer.getDirectBenefitsList().stream().map(DirectBenefitsDtoMapper::toDto).collect(Collectors.toList()));
        dto.setIndirectBenefitsList(customer.getIndirectBenefitsList().stream().map(IndirectBenefitsDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }
}
