package com.aubrun.eric.projet12.business.service.exception;

import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class WrongIdentifier extends RuntimeException {

    public WrongIdentifier() {
        super("Please verify your login details");
    }


}
