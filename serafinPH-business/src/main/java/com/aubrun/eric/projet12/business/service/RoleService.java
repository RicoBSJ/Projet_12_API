package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.business.dto.RoleDto;
import com.aubrun.eric.projet12.business.mapper.RoleDtoMapper;
import com.aubrun.eric.projet12.consumer.repository.NomenclatureUserRepository;
import com.aubrun.eric.projet12.consumer.repository.RoleRepository;
import com.aubrun.eric.projet12.model.ERole;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import com.aubrun.eric.projet12.model.Role;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RoleService {

    private final RoleRepository roleRepository;
    private final NomenclatureUserRepository nomenclatureUserRepository;

    public RoleService(RoleRepository roleRepository, NomenclatureUserRepository nomenclatureUserRepository) {
        this.roleRepository = roleRepository;
        this.nomenclatureUserRepository = nomenclatureUserRepository;
    }

    public List<RoleDto> findAll() {
        return roleRepository.findAll().stream().map(RoleDtoMapper::toDto).collect(Collectors.toList());
    }

    public void save(String lastName, RoleDto newRole) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findNomenclatureUserByLastName(lastName).orElseThrow(RuntimeException::new);
        String roleType = "ROLE_USER";
        ERole roleUserType = ERole.valueOf(roleType);
        Role role = new Role();
        role.setRoleId(newRole.getRoleId());
        role.setRoleName(roleUserType);
        roleRepository.save(role);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void update(RoleDto roleDto) {
        RoleDtoMapper.toDto(roleRepository.save(RoleDtoMapper.toEntity(roleDto)));
    }

    public RoleDto findById(int id) {
        return RoleDtoMapper.toDto(roleRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    public void deleteById(int id) {
        roleRepository.deleteById(id);
    }
}
