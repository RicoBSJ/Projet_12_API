package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.business.dto.IndirectBenefitsDto;
import com.aubrun.eric.projet12.business.mapper.IndirectBenefitsDtoMapper;
import com.aubrun.eric.projet12.consumer.repository.IndirectBenefitsRepository;
import com.aubrun.eric.projet12.consumer.repository.NomenclatureUserRepository;
import com.aubrun.eric.projet12.model.IndirectBenefits;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class IndirectBenefitsService {

    private final IndirectBenefitsRepository indirectBenefitsRepository;
    private final NomenclatureUserRepository nomenclatureUserRepository;

    public IndirectBenefitsService(IndirectBenefitsRepository indirectBenefitsRepository, NomenclatureUserRepository nomenclatureUserRepository) {
        this.indirectBenefitsRepository = indirectBenefitsRepository;
        this.nomenclatureUserRepository = nomenclatureUserRepository;
    }

    public List<IndirectBenefitsDto> findAll() {
        return indirectBenefitsRepository.findAll().stream().map(IndirectBenefitsDtoMapper::toDto).collect(Collectors.toList());
    }

    public IndirectBenefitsDto findById(int indirectBenefitsId) {
        return IndirectBenefitsDtoMapper.toDto(indirectBenefitsRepository.findById(indirectBenefitsId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, IndirectBenefitsDto indirectBenefitsDto) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findNomenclatureUserByLastName(lastName).orElseThrow(RuntimeException::new);
        IndirectBenefits indirectBenefits = new IndirectBenefits();
        indirectBenefits.setIndirectBenefitName(indirectBenefitsDto.getIndirectBenefitName());
        indirectBenefits.setIndirectBenefitType(indirectBenefitsDto.getIndirectBenefitType());
        indirectBenefitsRepository.save(indirectBenefits);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void update(IndirectBenefitsDto indirectBenefitsDto, int indirectBenefitsDtoId) {
        IndirectBenefits indirectBenefits = indirectBenefitsRepository.findById(indirectBenefitsDtoId).orElseThrow(RuntimeException::new);
        indirectBenefits.setIndirectBenefitName(indirectBenefitsDto.getIndirectBenefitName());
        indirectBenefits.setIndirectBenefitType(indirectBenefitsDto.getIndirectBenefitType());
        indirectBenefitsRepository.save(indirectBenefits);
    }

    public void deleteById(int indirectBenefitsDtoId){
        indirectBenefitsRepository.deleteById(indirectBenefitsDtoId);
    }
}
