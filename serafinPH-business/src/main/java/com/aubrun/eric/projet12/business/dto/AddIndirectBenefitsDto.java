package com.aubrun.eric.projet12.business.dto;

import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.util.Collections;
import java.util.List;

@ExcludeClassFromJacocoGeneratedReport
public class AddIndirectBenefitsDto {

    private List<Integer> indirectBenefits;

    public AddIndirectBenefitsDto(int indirectBenefits) {
        this.indirectBenefits = Collections.singletonList(indirectBenefits);
    }

    public List<Integer> getIndirectBenefits() {
        return indirectBenefits;
    }

    public void setIndirectBenefits(List<Integer> indirectBenefits) {
        this.indirectBenefits = indirectBenefits;
    }
}
