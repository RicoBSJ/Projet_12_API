package com.aubrun.eric.projet12.business.dto;

import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.util.Collections;
import java.util.List;

@ExcludeClassFromJacocoGeneratedReport
public class AddDirectBenefitsDto {

    private List<Integer> directBenefits;

    public AddDirectBenefitsDto(int directBenefits) {
        this.directBenefits = Collections.singletonList(directBenefits);
    }

    public List<Integer> getDirectBenefits() {
        return directBenefits;
    }

    public void setDirectBenefits(List<Integer> directBenefits) {
        this.directBenefits = directBenefits;
    }
}
