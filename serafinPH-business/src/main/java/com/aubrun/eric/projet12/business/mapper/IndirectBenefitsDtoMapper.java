package com.aubrun.eric.projet12.business.mapper;

import com.aubrun.eric.projet12.business.dto.IndirectBenefitsDto;
import com.aubrun.eric.projet12.model.IndirectBenefits;
import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class IndirectBenefitsDtoMapper {

    static public IndirectBenefitsDto toDto(IndirectBenefits indirectBenefits) {

        IndirectBenefitsDto dto = new IndirectBenefitsDto();
        dto.setIndirectBenefitsId(indirectBenefits.getIndirectBenefitsId());
        dto.setIndirectBenefitName(indirectBenefits.getIndirectBenefitName());
        dto.setIndirectBenefitType(indirectBenefits.getIndirectBenefitType());
        return dto;
    }

    static public IndirectBenefits toEntity(IndirectBenefitsDto indirectBenefitsDto) {

        IndirectBenefits entity = new IndirectBenefits();
        entity.setIndirectBenefitsId(indirectBenefitsDto.getIndirectBenefitsId());
        entity.setIndirectBenefitName(indirectBenefitsDto.getIndirectBenefitName());
        entity.setIndirectBenefitType(indirectBenefitsDto.getIndirectBenefitType());
        return entity;
    }
}
