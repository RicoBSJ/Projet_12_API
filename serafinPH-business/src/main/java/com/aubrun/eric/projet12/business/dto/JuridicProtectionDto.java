package com.aubrun.eric.projet12.business.dto;

import com.aubrun.eric.projet12.model.EJuridicProtection;
import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class JuridicProtectionDto {

    private Integer juridicProtectionId;
    private EJuridicProtection juridicProtectionName;

    public Integer getJuridicProtectionId() {
        return juridicProtectionId;
    }

    public void setJuridicProtectionId(Integer juridicProtectionId) {
        this.juridicProtectionId = juridicProtectionId;
    }

    public EJuridicProtection getJuridicProtectionName() {
        return juridicProtectionName;
    }

    public void setJuridicProtectionName(EJuridicProtection juridicProtectionName) {
        this.juridicProtectionName = juridicProtectionName;
    }
}
