package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.business.dto.DirectBenefitsDto;
import com.aubrun.eric.projet12.business.mapper.DirectBenefitsDtoMapper;
import com.aubrun.eric.projet12.consumer.repository.DirectBenefitsRepository;
import com.aubrun.eric.projet12.consumer.repository.NomenclatureUserRepository;
import com.aubrun.eric.projet12.model.DirectBenefits;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class DirectBenefitsService {

    private final DirectBenefitsRepository directBenefitsRepository;
    private final NomenclatureUserRepository nomenclatureUserRepository;

    public DirectBenefitsService(DirectBenefitsRepository directBenefitsRepository, NomenclatureUserRepository nomenclatureUserRepository) {
        this.directBenefitsRepository = directBenefitsRepository;
        this.nomenclatureUserRepository = nomenclatureUserRepository;
    }

    public List<DirectBenefitsDto> findAll() {
        return directBenefitsRepository.findAll().stream().map(DirectBenefitsDtoMapper::toDto).collect(Collectors.toList());
    }

    public DirectBenefitsDto findById(int directBenefitsId) {
        return DirectBenefitsDtoMapper.toDto(directBenefitsRepository.findById(directBenefitsId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, DirectBenefitsDto directBenefitsDto) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findNomenclatureUserByLastName(lastName).orElseThrow(RuntimeException::new);
        DirectBenefits directBenefits = new DirectBenefits();
        directBenefits.setDirectBenefitName(directBenefitsDto.getDirectBenefitName());
        directBenefits.setDirectBenefitType(directBenefitsDto.getDirectBenefitType());
        directBenefitsRepository.save(directBenefits);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void update(DirectBenefitsDto directBenefitsDto, int directBenefitsId) {
        DirectBenefits directBenefits = directBenefitsRepository.findById(directBenefitsId).orElseThrow(RuntimeException::new);
        directBenefits.setDirectBenefitName(directBenefitsDto.getDirectBenefitName());
        directBenefits.setDirectBenefitType(directBenefitsDto.getDirectBenefitType());
        directBenefitsRepository.save(directBenefits);
    }

    public void deleteById(int directBenefitsId){
        directBenefitsRepository.deleteById(directBenefitsId);
    }
}
