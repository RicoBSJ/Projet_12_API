package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.business.dto.NomenclatureUserDto;
import com.aubrun.eric.projet12.business.mapper.NomenclatureUserDtoMapper;
import com.aubrun.eric.projet12.consumer.repository.CustomerRepository;
import com.aubrun.eric.projet12.consumer.repository.EstablishmentRepository;
import com.aubrun.eric.projet12.consumer.repository.NomenclatureUserRepository;
import com.aubrun.eric.projet12.consumer.repository.RoleRepository;
import com.aubrun.eric.projet12.model.Customer;
import com.aubrun.eric.projet12.model.Establishment;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import com.aubrun.eric.projet12.model.Role;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class NomenclatureUserService {

    private final NomenclatureUserRepository nomenclatureUserRepository;
    private final EstablishmentRepository establishmentRepository;
    private final RoleRepository roleRepository;
    private final CustomerRepository customerRepository;

    public NomenclatureUserService(NomenclatureUserRepository nomenclatureUserRepository, EstablishmentRepository establishmentRepository, RoleRepository roleRepository, CustomerRepository customerRepository) {
        this.nomenclatureUserRepository = nomenclatureUserRepository;
        this.establishmentRepository = establishmentRepository;
        this.roleRepository = roleRepository;
        this.customerRepository = customerRepository;
    }

    public List<NomenclatureUserDto> findAll(){
        return nomenclatureUserRepository.findAll().stream().map(NomenclatureUserDtoMapper::toDto).collect(Collectors.toList());
    }

    public void save(NomenclatureUserDto newUser){
        NomenclatureUser nomenclatureUser = new NomenclatureUser();
        nomenclatureUser.setFirstName(newUser.getFirstName());
        nomenclatureUser.setLastName(newUser.getLastName());
        nomenclatureUser.setDateBirthUser(newUser.getDateBirthUser());
        nomenclatureUser.setSocialSecurityNumberUser(newUser.getSocialSecurityNumberUser());
        nomenclatureUser.setPhoneUser(newUser.getPhoneUser());
        nomenclatureUser.setEntryDateUser(newUser.getEntryDateUser());
        nomenclatureUser.setAgeUser(newUser.getAgeUser());
        nomenclatureUser.setPassword(newUser.getPassword());
        nomenclatureUser.setEmail(newUser.getEmail());
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public NomenclatureUserDto findById(int id){
        return NomenclatureUserDtoMapper.toDto(nomenclatureUserRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    public void updateNomenclatureUser(int nomenclatureUserId, LocalDateTime releaseDateUser) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findById(nomenclatureUserId).orElseThrow(RuntimeException::new);
        nomenclatureUser.setReleaseDateUser(releaseDateUser);
        List<Customer> customers = nomenclatureUser.getCustomerList().stream().peek(c -> c.setNomenclatureUser(null)).toList();
        nomenclatureUser.setCustomerList(new ArrayList<>());
        nomenclatureUserRepository.save(nomenclatureUser);
        customerRepository.saveAll(customers);
    }

    public void addEstablishment(int nomenclatureUserId, List<Integer> establishment){
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findById(nomenclatureUserId).orElseThrow(RuntimeException::new);
        List<Establishment> establishmentList = establishmentRepository.findAllById(establishment);
        nomenclatureUser.getEstablishments().addAll(establishmentList);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void addRole(int nomenclatureUserId, List<Integer> roles){
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findById(nomenclatureUserId).orElseThrow(RuntimeException::new);
        List<Role> roleList = roleRepository.findAllById(roles);
        nomenclatureUser.getRoles().addAll(roleList);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void delete(int id){
        nomenclatureUserRepository.deleteById(id);
    }

    public void deleteEstablishment(int nomenclatureUserId){
        nomenclatureUserRepository.delEstablishment(nomenclatureUserId);
    }

    public void deleteRole(int nomenclatureUserId){
        nomenclatureUserRepository.delRole(nomenclatureUserId);
    }
}
