package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.business.dto.NeedsDto;
import com.aubrun.eric.projet12.business.mapper.NeedsDtoMapper;
import com.aubrun.eric.projet12.consumer.repository.NeedsRepository;
import com.aubrun.eric.projet12.consumer.repository.NomenclatureUserRepository;
import com.aubrun.eric.projet12.model.Needs;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class NeedsService {

    private final NeedsRepository needsRepository;
    private final NomenclatureUserRepository nomenclatureUserRepository;

    public NeedsService(NeedsRepository needsRepository, NomenclatureUserRepository nomenclatureUserRepository) {
        this.needsRepository = needsRepository;
        this.nomenclatureUserRepository = nomenclatureUserRepository;
    }

    public List<NeedsDto> findAll() {
        return needsRepository.findAll().stream().map(NeedsDtoMapper::toDto).collect(Collectors.toList());
    }

    public NeedsDto findById(int needsId) {
        return NeedsDtoMapper.toDto(needsRepository.findById(needsId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, NeedsDto needsDto) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findNomenclatureUserByLastName(lastName).orElseThrow(RuntimeException::new);
        Needs needs = new Needs();
        needs.setNeedName(needsDto.getNeedName());
        needs.setNeedType(needsDto.getNeedType());
        needsRepository.save(needs);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void update(NeedsDto needsDto, int needsDtoId) {
        Needs needs = needsRepository.findById(needsDtoId).orElseThrow(RuntimeException::new);
        needs.setNeedName(needsDto.getNeedName());
        needs.setNeedType(needsDto.getNeedType());
        needsRepository.save(needs);
    }

    public void deleteById(int needsDtoId){
        needsRepository.deleteById(needsDtoId);
    }
}
