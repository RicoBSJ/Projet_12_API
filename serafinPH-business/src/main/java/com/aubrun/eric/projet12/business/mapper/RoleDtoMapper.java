package com.aubrun.eric.projet12.business.mapper;

import com.aubrun.eric.projet12.business.dto.RoleDto;
import com.aubrun.eric.projet12.model.Role;
import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class RoleDtoMapper {

    static public RoleDto toDto(Role role) {

        RoleDto dto = new RoleDto();
        dto.setRoleId(role.getRoleId());
        dto.setRoleName(role.getRoleName());
        return dto;
    }

    static public Role toEntity(RoleDto roleDto) {

        Role entity = new Role();
        entity.setRoleId(roleDto.getRoleId());
        entity.setRoleName(roleDto.getRoleName());
        return entity;
    }
}
