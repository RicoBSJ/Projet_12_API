package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.business.dto.JuridicProtectionDto;
import com.aubrun.eric.projet12.business.mapper.JuridicProtectionDtoMapper;
import com.aubrun.eric.projet12.consumer.repository.JuridicProtectionRepository;
import com.aubrun.eric.projet12.consumer.repository.NomenclatureUserRepository;
import com.aubrun.eric.projet12.model.JuridicProtection;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class JuridicProtectionService {

    private final JuridicProtectionRepository juridicProtectionRepository;
    private final NomenclatureUserRepository nomenclatureUserRepository;

    public JuridicProtectionService(JuridicProtectionRepository juridicProtectionRepository, NomenclatureUserRepository nomenclatureUserRepository) {
        this.juridicProtectionRepository = juridicProtectionRepository;
        this.nomenclatureUserRepository = nomenclatureUserRepository;
    }

    public List<JuridicProtectionDto> findAll() {
        return juridicProtectionRepository.findAll().stream().map(JuridicProtectionDtoMapper::toDto).collect(Collectors.toList());
    }

    public JuridicProtectionDto findById(int juridicProtectionId) {
        return JuridicProtectionDtoMapper.toDto(juridicProtectionRepository.findById(juridicProtectionId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, JuridicProtectionDto juridicProtectionDto) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findNomenclatureUserByLastName(lastName).orElseThrow(RuntimeException::new);
        JuridicProtection juridicProtection = new JuridicProtection();
        juridicProtection.setJuridicProtectionName(juridicProtectionDto.getJuridicProtectionName());
        juridicProtectionRepository.save(juridicProtection);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void update(JuridicProtectionDto juridicProtectionDto, int juridicProtectionDtoId) {
        JuridicProtection juridicProtection = juridicProtectionRepository.findById(juridicProtectionDtoId).orElseThrow(RuntimeException::new);
        juridicProtection.setJuridicProtectionName(juridicProtectionDto.getJuridicProtectionName());
        juridicProtectionRepository.save(juridicProtection);
    }

    public void deleteById(int juridicProtectionId){
        juridicProtectionRepository.deleteById(juridicProtectionId);
    }
}
