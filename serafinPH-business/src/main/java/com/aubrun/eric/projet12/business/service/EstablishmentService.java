package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.business.dto.EstablishmentDto;
import com.aubrun.eric.projet12.business.mapper.EstablishmentDtoMapper;
import com.aubrun.eric.projet12.consumer.repository.EstablishmentRepository;
import com.aubrun.eric.projet12.consumer.repository.NomenclatureUserRepository;
import com.aubrun.eric.projet12.model.Establishment;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class EstablishmentService {

    private final EstablishmentRepository establishmentRepository;
    private final NomenclatureUserRepository nomenclatureUserRepository;

    public EstablishmentService(EstablishmentRepository establishmentRepository, NomenclatureUserRepository nomenclatureUserRepository) {
        this.establishmentRepository = establishmentRepository;
        this.nomenclatureUserRepository = nomenclatureUserRepository;
    }

    public List<EstablishmentDto> findAll() {
        return establishmentRepository.findAll().stream().map(EstablishmentDtoMapper::toDto).collect(Collectors.toList());
    }

    public EstablishmentDto findById(int establishmentId) {
        return EstablishmentDtoMapper.toDto(establishmentRepository.findById(establishmentId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, EstablishmentDto establishmentDto) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findNomenclatureUserByLastName(lastName).orElseThrow(RuntimeException::new);
        Establishment establishment = new Establishment();
        establishment.setEstablishmentName(establishmentDto.getEstablishmentName());
        establishmentRepository.save(establishment);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void update(EstablishmentDto establishmentDto, int establishmentDtoId) {
        Establishment establishment = establishmentRepository.findById(establishmentDtoId).orElseThrow(RuntimeException::new);
        establishment.setEstablishmentName(establishmentDto.getEstablishmentName());
        establishmentRepository.save(establishment);
    }

    public void deleteById(int establishmentDtoId){
        establishmentRepository.deleteById(establishmentDtoId);
    }
}
