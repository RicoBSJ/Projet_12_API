package com.aubrun.eric.projet12.business.dto;

import com.aubrun.eric.projet12.model.EDirectBenefits;
import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class DirectBenefitsDto {

    private Integer directBenefitsId;
    private String directBenefitName;
    private EDirectBenefits directBenefitType;

    public Integer getDirectBenefitsId() {
        return directBenefitsId;
    }

    public void setDirectBenefitsId(Integer directBenefitsId) {
        this.directBenefitsId = directBenefitsId;
    }

    public String getDirectBenefitName() {
        return directBenefitName;
    }

    public void setDirectBenefitName(String directBenefitName) {
        this.directBenefitName = directBenefitName;
    }

    public EDirectBenefits getDirectBenefitType() {
        return directBenefitType;
    }

    public void setDirectBenefitType(EDirectBenefits directBenefitType) {
        this.directBenefitType = directBenefitType;
    }
}
