package com.aubrun.eric.projet12.business.mapper;

import com.aubrun.eric.projet12.business.dto.DirectBenefitsDto;
import com.aubrun.eric.projet12.model.DirectBenefits;
import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class DirectBenefitsDtoMapper {

    static public DirectBenefitsDto toDto(DirectBenefits directBenefits) {

        DirectBenefitsDto dto = new DirectBenefitsDto();
        dto.setDirectBenefitsId(directBenefits.getDirectBenefitsId());
        dto.setDirectBenefitName(directBenefits.getDirectBenefitName());
        dto.setDirectBenefitType(directBenefits.getDirectBenefitType());
        return dto;
    }

    static public DirectBenefits toEntity(DirectBenefitsDto directBenefitsDto) {

        DirectBenefits entity = new DirectBenefits();
        entity.setDirectBenefitsId(directBenefitsDto.getDirectBenefitsId());
        entity.setDirectBenefitName(directBenefitsDto.getDirectBenefitName());
        entity.setDirectBenefitType(directBenefitsDto.getDirectBenefitType());
        return entity;
    }
}
