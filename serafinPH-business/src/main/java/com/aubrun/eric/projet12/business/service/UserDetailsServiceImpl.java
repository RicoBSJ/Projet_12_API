package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.consumer.repository.NomenclatureUserRepository;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@ExcludeClassFromJacocoGeneratedReport
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final NomenclatureUserRepository nomenclatureUserRepository;

    public UserDetailsServiceImpl(NomenclatureUserRepository nomenclatureUserRepository) {
        this.nomenclatureUserRepository = nomenclatureUserRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        NomenclatureUser user = nomenclatureUserRepository.findNomenclatureUserByEmail(email).orElseThrow(RuntimeException::new);
        return UserDetailsImpl.build(user);
    }
}
