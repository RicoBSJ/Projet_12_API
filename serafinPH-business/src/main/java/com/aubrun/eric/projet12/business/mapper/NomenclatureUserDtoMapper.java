package com.aubrun.eric.projet12.business.mapper;

import com.aubrun.eric.projet12.business.dto.NomenclatureUserDto;
import com.aubrun.eric.projet12.model.NomenclatureUser;
import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.util.stream.Collectors;

@ExcludeClassFromJacocoGeneratedReport
public class NomenclatureUserDtoMapper {

    static public NomenclatureUserDto toDto(NomenclatureUser nomenclatureUser) {

        NomenclatureUserDto dto = new NomenclatureUserDto();
        dto.setNomenclatureUserId(nomenclatureUser.getNomenclatureUserId());
        dto.setFirstName(nomenclatureUser.getFirstName());
        dto.setLastName(nomenclatureUser.getLastName());
        dto.setPassword(nomenclatureUser.getPassword());
        dto.setEmail(nomenclatureUser.getEmail());
        dto.setPhoneUser(nomenclatureUser.getPhoneUser());
        dto.setDateBirthUser(nomenclatureUser.getDateBirthUser());
        dto.setSocialSecurityNumberUser(nomenclatureUser.getSocialSecurityNumberUser());
        dto.setEntryDateUser(nomenclatureUser.getEntryDateUser());
        dto.setReleaseDateUser(nomenclatureUser.getReleaseDateUser());
        dto.setAgeUser(nomenclatureUser.getAgeUser());
        dto.setCustomerList(nomenclatureUser.getCustomerList().stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList()));
        dto.setRoles(nomenclatureUser.getRoles().stream().map(RoleDtoMapper::toDto).collect(Collectors.toSet()));
        dto.setEstablishments(nomenclatureUser.getEstablishments().stream().map(EstablishmentDtoMapper::toDto).collect(Collectors.toSet()));
        return dto;
    }

    static public NomenclatureUser toEntity(NomenclatureUserDto nomenclatureUserDto) {

        NomenclatureUser entity = new NomenclatureUser();
        entity.setNomenclatureUserId(nomenclatureUserDto.getNomenclatureUserId());
        entity.setFirstName(nomenclatureUserDto.getFirstName());
        entity.setLastName(nomenclatureUserDto.getLastName());
        entity.setPassword(nomenclatureUserDto.getPassword());
        entity.setEmail(nomenclatureUserDto.getEmail());
        entity.setPhoneUser(nomenclatureUserDto.getPhoneUser());
        entity.setDateBirthUser(nomenclatureUserDto.getDateBirthUser());
        entity.setSocialSecurityNumberUser(nomenclatureUserDto.getSocialSecurityNumberUser());
        entity.setEntryDateUser(nomenclatureUserDto.getEntryDateUser());
        entity.setReleaseDateUser(nomenclatureUserDto.getReleaseDateUser());
        entity.setAgeUser(nomenclatureUserDto.getAgeUser());
        entity.setRoles(nomenclatureUserDto.getRoles().stream().map(RoleDtoMapper::toEntity).collect(Collectors.toSet()));
        entity.setEstablishments(nomenclatureUserDto.getEstablishments().stream().map(EstablishmentDtoMapper::toEntity).collect(Collectors.toSet()));
        return entity;
    }
}
