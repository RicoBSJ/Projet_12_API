package com.aubrun.eric.projet12.business.mapper;

import com.aubrun.eric.projet12.business.dto.NeedsDto;
import com.aubrun.eric.projet12.model.Needs;
import com.aubrun.eric.projet12.model.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class NeedsDtoMapper {

    static public NeedsDto toDto(Needs needs) {

        NeedsDto dto = new NeedsDto();
        dto.setNeedsId(needs.getNeedsId());
        dto.setNeedName(needs.getNeedName());
        dto.setNeedType(needs.getNeedType());
        return dto;
    }

    static public Needs toEntity(NeedsDto needsDto) {

        Needs entity = new Needs();
        entity.setNeedsId(needsDto.getNeedsId());
        entity.setNeedName(needsDto.getNeedName());
        entity.setNeedType(needsDto.getNeedType());
        return entity;
    }
}
