package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.business.dto.CustomerDto;
import com.aubrun.eric.projet12.business.dto.SearchCustomerDto;
import com.aubrun.eric.projet12.business.mapper.*;
import com.aubrun.eric.projet12.consumer.repository.*;
import com.aubrun.eric.projet12.model.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final NomenclatureUserRepository nomenclatureUserRepository;
    private final EstablishmentRepository establishmentRepository;
    private final JuridicProtectionRepository juridicProtectionRepository;
    private final NeedsRepository needsRepository;
    private final DirectBenefitsRepository directBenefitsRepository;
    private final IndirectBenefitsRepository indirectBenefitsRepository;
    private final SearchRepository searchRepository;

    public CustomerService(CustomerRepository customerRepository, NomenclatureUserRepository nomenclatureUserRepository, EstablishmentRepository establishmentRepository, JuridicProtectionRepository juridicProtectionRepository, NeedsRepository needsRepository, DirectBenefitsRepository directBenefitsRepository, IndirectBenefitsRepository indirectBenefitsRepository, SearchRepository searchRepository) {
        this.customerRepository = customerRepository;
        this.nomenclatureUserRepository = nomenclatureUserRepository;
        this.establishmentRepository = establishmentRepository;
        this.juridicProtectionRepository = juridicProtectionRepository;
        this.needsRepository = needsRepository;
        this.directBenefitsRepository = directBenefitsRepository;
        this.indirectBenefitsRepository = indirectBenefitsRepository;
        this.searchRepository = searchRepository;
    }

    public List<CustomerDto> findCustomersModerator() {
        return customerRepository.findAll().stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList());
    }

    public List<CustomerDto> findAll(Principal principal) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findNomenclatureUserByEmail(principal.getName()).orElseThrow(RuntimeException::new);
        return customerRepository.findAllByNomenclatureUser(nomenclatureUser).stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList());
    }

    public void save(CustomerDto addCustomerDto, int nomenclatureUserId) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findById(nomenclatureUserId).orElseThrow(RuntimeException::new);
        Customer customer = new Customer();
        customer.setCustomerFirstName(addCustomerDto.getCustomerFirstName());
        customer.setCustomerLastName(addCustomerDto.getCustomerLastName());
        customer.setDateBirth(addCustomerDto.getDateBirth());
        customer.setSocialSecurityNumber(addCustomerDto.getSocialSecurityNumber());
        customer.setMutualName(addCustomerDto.getMutualName());
        customer.setEntryDate(addCustomerDto.getEntryDate());
        customer.setAge(addCustomerDto.getAge());
        customer.setNomenclatureUser(nomenclatureUser);
        Customer customerSave = customerRepository.save(customer);
        nomenclatureUser.getCustomerList().add(customerSave);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void addNomenclatureUser(int nomenclatureUserId, int customerId) {
        NomenclatureUser nomenclatureUser = nomenclatureUserRepository.findById(nomenclatureUserId).orElseThrow(RuntimeException::new);
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customer.setNomenclatureUser(nomenclatureUser);
        nomenclatureUser.getCustomerList().add(customer);
        customerRepository.save(customer);
        nomenclatureUserRepository.save(nomenclatureUser);
    }

    public void addNeeds(int customerId, List<Integer> needs) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        List<Needs> needsList = needsRepository.findAllById(needs);
        customer.getNeedsList().addAll(needsList);
        customerRepository.save(customer);
    }

    public void addDirectBenefits(int customerId, List<Integer> directBenefits) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        List<DirectBenefits> directBenefitsList = directBenefitsRepository.findAllById(directBenefits);
        customer.getDirectBenefitsList().addAll(directBenefitsList);
        customerRepository.save(customer);
    }

    public void addIndirectBenefits(int customerId, List<Integer> indirectBenefits) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        List<IndirectBenefits> indirectBenefitsList = indirectBenefitsRepository.findAllById(indirectBenefits);
        customer.getIndirectBenefitsList().addAll(indirectBenefitsList);
        customerRepository.save(customer);
    }

    public void addEstablishment(int customerId, List<Integer> establishment) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        List<Establishment> establishmentList = establishmentRepository.findAllById(establishment);
        customer.getEstablishmentList().addAll(establishmentList);
        customerRepository.save(customer);
    }

    public void addJuridicProtections(int customerId, List<Integer> juridicProtection) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        List<JuridicProtection> juridicProtectionList = juridicProtectionRepository.findAllById(juridicProtection);
        customer.getJuridicProtectionList().addAll(juridicProtectionList);
        customerRepository.save(customer);
    }

    public void updateCustomer(int customerId, LocalDateTime releaseDate) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customer.setReleaseDate(releaseDate);
        if (customer.getNomenclatureUser() != null){
            customer.getNomenclatureUser().getCustomerList().remove(customer);
        }
        customer.setNomenclatureUser(null);
        customerRepository.save(customer);
    }

    public void deleteByJuridicProtections(int customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customerRepository.deleteByJuridicProtectionList(customer.getCustomerId());
    }

    public void deleteByEstablishments(int customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customerRepository.deleteByEstablishmentList(customer.getCustomerId());
    }

    public void deleteNeeds(int customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customerRepository.deleteByNeedsList(customer.getCustomerId());
    }

    public void deleteDirectBenefits(int customerId){
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customerRepository.deleteByDirectBenefits(customer.getCustomerId());
    }

    public void deleteIndirectBenefits(int customerId){
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customerRepository.deleteByIndirectBenefits(customer.getCustomerId());
    }

    public CustomerDto findById(Integer id) {
        return CustomerDtoMapper.toDto(customerRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    public List<CustomerDto> searchCustomer(SearchCustomerDto searchCustomerDto){
        SearchCustomer searchCustomer = SearchCustomerDtoMapper.toEntity(searchCustomerDto);
        return searchRepository.findAllByNameAndDateBirthAndEntryDateAndAge(searchCustomer).stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList());
    }
}
