Ce projet correspond à l'API de mon projet 12 dans l'ancien parcours de DA Java/JEE :

* JDK 16.0.1
* Docker version 20.10.22
* Hibernate Validator 6.2.0.Final
* PostgreSQL 42.5.0
* Git version 2.23.0
* Spring Boot version 2.4.2

Prérequis
-----

Il est indispensable d'installer Docker et de le démarrer.

Voir le site référence [Docker website](http://www.docker.io/gettingstarted/#h_installation) pour les instructions d'installation.

Construire
-----

Les différentes étapes pour construire les images Docker :

1. Cloner le dépôt du frontend :

        git clone https://gitlab.com/RicoBSJ/projet_12_mvc.git

2. Construire l'image du frontend :

        cd projet_12_mvc/
        cd serafinPH/
        cd serafinPH-webapp/
        docker build -t serafinphmvc .

   C'est assez rapide.


3. Cloner ce dépôt :

        Il faut revenir dans la racine du répertoire
        git clone https://gitlab.com/RicoBSJ/Projet_12_API.git

4. Construire l'image de la base de données PostgreSQL :

        cd Projet_12_API/
        cd serafinPH-webapp/
        cd src
        cd main
        cd resources
        docker build -t dockerdb .

      C'est assez rapide.


3. Construire l'image de l'API' :

        cd ..
        cd ..
        cd .. (il faut se positionner dans Projet_12_API/serafinPH-webapp/)
        docker build -t serafinphapi .

      C'est assez rapide.


4. Démarrer les 3 images construites ci-dessus :

        docker-compose up

      C'est un peu plus long que la construction des images.


5. Une fois les containers démarrés vous devriez pouvoir accéder à l'application web via [http://localhost:8080/serafinPH-webapp/](http://localhost:8080/serafinPH-webapp/) sur votre machine.
      
       ouvrir http://localhost:8080/serafinPH-webapp/
