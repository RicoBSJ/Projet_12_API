CREATE SEQUENCE IF NOT EXISTS nomenclature_user_id_seq;

insert into nomenclature_user(

    id_nomenclature_user,
    nomenclature_user_first_name,
    nomenclature_user_last_name,
    nomenclature_user_password,
    nomenclature_user_email,
    phone_user,
    date_birth_user,
    social_security_number_user,
    entry_date_user,
    age_user
)
values
    (nextval('nomenclature_user_id_seq'), 'Steve', 'JOBS', '$2a$10$fcEe3X.fC7QxmSj7hRZJdujuZ8UavlkhewM37rcjJWdRtQt.ROJPK','steve.jobs@gmail.com', '0677645532', '1953-02-20', '165021304702154', '2019-05-27 00:00:00', 69),
    (nextval('nomenclature_user_id_seq'), 'Elon', 'MUSK', '$2a$10$PBcySTS3eGN69mRzF6BWx.hs7aayN0FIJqLY16ZsKen4S0ikAyL9G', 'elon.musk@free.fr', '0676647530', '1986-02-20', '186021304702154', '2017-04-20 00:00:00', 36),
    (nextval('nomenclature_user_id_seq'), 'Jack', 'MA', '$2a$10$c/Iju8gfO4wokM2nY8B6V.aJP9plWIgSJ.4xzrj27UZQ5TP9cxeeq', 'jack.ma@free.fr', '0677445832', '1968-02-20', '168021304702154', '2015-01-20 00:00:00', 54),
    (nextval('nomenclature_user_id_seq'), 'Jeff', 'BEZOS', '$2a$10$oRStZm22E3KTYZop3vBB9.sKDnzv13YYgAwHJrQ1J8/x8T.k7tQAK', 'jeff.bezos@gmail.com', '0777485833', '1968-08-27', '168081304702154', '2015-01-20 00:00:00', 54),
    (nextval('nomenclature_user_id_seq'), 'Philip', 'KNIGHT', '$2a$10$P0.aPdbYOpdtquVx6JQ01Otl43eA.uy9gmgxgOvHq193Zy3LF0VkG', 'philip.knight@free.fr', '0677445831', '1960-08-31', '1600821304702154', '2007-09-17 00:00:00', 62),
    (nextval('nomenclature_user_id_seq'), 'Mark', 'ZUCKERBERG', '$2a$10$d65Cvz.Uf3MYTy2tu5W4Eun3Tzckcgm23yEfcZD/xPH7oTPjNy2ma', 'mark.zuckerberg@free.fr', '0676847537', '1979-04-10', '179041304702154', '2008-04-20 00:00:00', 43),
    (nextval('nomenclature_user_id_seq'), 'William Zebulon', 'FOSTER', '$2a$10$dG0tU0KM0GTUm4xs56UnDeKuxwkRA6DSzpI5WYoNk/.l294zERTNe', 'william_zebulon.foster@gmail.com', '0670445831', '1956-08-31', '1560821304702154', '2010-09-17 00:00:00', 66),
    (nextval('nomenclature_user_id_seq'), 'Antonio', 'GRAMSCI', '$2a$10$vhMKl55c6q/78r3s5w9OI.fgVh366UaUPRb13LqdTr4jPzA2d7NfS', 'antonio.gramsci@free.fr', '0777445832', '1970-02-20', '170021304702154', '2012-01-20 00:00:00', 52),
    (nextval('nomenclature_user_id_seq'), 'Martin', 'HEIDEGGER', '$2a$10$Mm/u35cmpZzyR.Fg6ntmq.YlIFs5WPv.cEghq30FmtYwrwvPjp3ze', 'martin.heidegger@free.fr', '0676817537', '1966-04-10', '166041304702154', '2000-04-20 00:00:00', 56),
    (nextval('nomenclature_user_id_seq'), 'Andrew', 'COHEN', '$2a$10$CZBukW/NfpkF/mON0f7dc.MtE95U72zWgjjyupFgDAo8d8mRN3QHi', 'andrew.cohen@gmail.com', '0676847037', '1975-04-10', '175041304702154', '2005-04-20 00:00:00', 47);

CREATE SEQUENCE IF NOT EXISTS customer_id_seq;

insert into customer(
    customer_id,
    age_customer,
    customer_first_name,
    customer_last_name,
    date_birth,
    social_security_number,
    mutual_name,
    entry_date,
    nomenclature_user,
    custom_list
)
values
    (nextval('customer_id_seq'), 20, 'Bob', 'SERE', '1998-12-28', '102011304702159', 'AZUR', '2019-05-27 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 30, 'Manuel', 'FRED', '1992-04-21', '192011304702159', 'MUTUELLE_DE_FRANCE', '2021-05-18 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 25, 'Sacha', 'GABR', '1997-04-16', '197011304702159', 'MUTUELLE_DU_NORD', '2020-10-30 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 40, 'Anne-Marie', 'GRED', '1974-12-16', '282011304702159', 'MUTUELLE_PACA', '2000-08-03 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 40, 'Joël', 'COUT', '1977-05-13', '182011304702159', 'ALPES_MUTUELLE', '2019-10-21 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 30, 'Pierre', 'SADR', '1980-05-29', '192011304702159', 'MUTUELLE_BUDGET', '2020-10-26 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 50, 'Kévin', 'GRAB', '1967-06-25', '172011304702159', 'PYRENEES_MUTUELLE', '2015-09-19 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 58, 'Geneviève', 'ALTRE', '1962-05-15', '264011304702159', 'MUTUELLE_POUR_TOUS', '2005-11-12 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 35, 'Karine', 'MOIN', '1977-04-24', '287011304702159', 'MUTUELLE_MIDI', '2010-05-09 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 50, 'Bernard', 'SANT', '1967-11-14', '172011304702159', 'MUTUELLE_GREASQUE', '2015-09-15 00:00:00', 10, 10),
    (nextval('customer_id_seq'), 20, 'Blandine', 'STRE', '1998-12-28', '202011304702159', 'MUTUELLE_DE_FRANCE', '2019-05-27 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 30, 'Maëlle', 'MOLI', '1992-04-21', '292011304702159', 'MUTUELLE_DU_NORD', '2021-05-18 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 25, 'Arthur', 'DRES', '1997-04-16', '197011304702159', 'MUTUELLE_PACA', '2020-10-30 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 40, 'Jean-François', 'NOIL', '1974-12-16', '182011304702159', 'ALPES_MUTUELLE', '2000-08-03 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 40, 'Françoise', 'CRIT', '1977-05-13', '282011304702159', 'MUTUELLE_BUDGET', '2019-10-21 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 30, 'Samuel', 'JAST', '1980-05-29', '192011304702159', 'PYRENEES_MUTUELLE', '2020-10-26 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 50, 'Gwenaëlle', 'HOIT', '1967-06-25', '272011304702159', 'MUTUELLE_POUR_TOUS', '2015-09-19 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 58, 'Gaspard', 'VUTR', '1962-05-15', '164011304702159', 'MUTUELLE_MIDI', '2005-11-12 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 35, 'Séverine', 'DRAT', '1977-04-24', '287011304702159', 'MUTUELLE_GREASQUE', '2010-05-09 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 50, 'Steve', 'GROI', '1967-11-14', '172011304702159', 'AZUR', '2015-09-15 00:00:00', 10, 10),
    (nextval('customer_id_seq'), 20, 'Jonathan', 'MORT', '1998-12-28', '102011304702159', 'MUTUELLE_DU_NORD', '2019-05-27 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 30, 'Albin', 'GRAT', '1992-04-21', '192011304702159', 'MUTUELLE_PACA', '2021-05-18 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 25, 'Sophia', 'MELA', '1997-04-16', '297011304702159', 'ALPES_MUTUELLE', '2020-10-30 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 40, 'Lionel', 'BAND', '1974-12-16', '182021304702159', 'MUTUELLE_BUDGET', '2000-08-03 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 40, 'Frédéric', 'ASTE', '1977-05-13', '182011304702159', 'PYRENEES_MUTUELLE', '2019-10-21 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 30, 'Gabriel', 'SONT', '1980-05-29', '192011304702159', 'MUTUELLE_POUR_TOUS', '2020-10-26 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 50, 'Maxime', 'FRET', '1967-06-25', '172011304702159', 'MUTUELLE_MIDI', '2015-09-19 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 58, 'Sylvia', 'ADRE', '1962-05-15', '264011304702159', 'MUTUELLE_GREASQUE', '2005-11-12 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 35, 'Muriel', 'LONT', '1977-04-24', '287011304702159', 'AZUR', '2010-05-09 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 50, 'Ange', 'NICR', '1967-11-14', '172011304702159', 'MUTUELLE_DE_FRANCE', '2015-09-15 00:00:00', 10, 10),
    (nextval('customer_id_seq'), 20, 'Baptiste', 'RETI', '1998-12-28', '102011304702159', 'ALPES_MUTUELLE', '2019-05-27 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 30, 'Amanda', 'NOIT', '1992-04-21', '292011304702159', 'MUTUELLE_BUDGET', '2021-05-18 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 25, 'Aïcha', 'POIL', '1997-04-16', '297011304702159', 'PYRENEES_MUTUELLE', '2020-10-30 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 40, 'Jane', 'SART', '1974-12-16', '282021304702159', 'MUTUELLE_POUR_TOUS', '2000-08-03 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 40, 'Marie', 'BRAD', '1977-05-13', '282011304702159', 'MUTUELLE_MIDI', '2019-10-21 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 30, 'Marc', 'ERTI', '1980-05-29', '192011304702159', 'MUTUELLE_GREASQUE', '2020-10-26 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 50, 'Rachel', 'BASS', '1967-06-25', '272011304702159', 'AZUR', '2015-09-19 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 58, 'Emmanuel', 'HUTE', '1962-05-15', '164011304702159', 'MUTUELLE_DE_FRANCE', '2005-11-12 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 35, 'Catherine', 'VROI', '1977-04-24', '287011304702159', 'MUTUELLE_DU_NORD', '2010-05-09 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 50, 'Amal', 'GRUT', '1967-11-14', '172011304702159', 'MUTUELLE_PACA', '2015-09-15 00:00:00', 10, 10),
    (nextval('customer_id_seq'), 20, 'Martine', 'DRIS', '1998-12-28', '102011304702159', 'MUTUELLE_BUDGET', '2019-05-27 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 30, 'Sonia', 'BONN', '1992-04-21', '292011304702159', 'PYRENEES_MUTUELLE', '2021-05-18 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 25, 'Sandrine', 'ARTE', '1997-04-16', '297011304702159', 'MUTUELLE_POUR_TOUS', '2020-10-30 00:00:00', 1, 1),
    (nextval('customer_id_seq'), 40, 'Marielle', 'DRUS', '1974-12-16', '282021304702159', 'MUTUELLE_MIDI', '2000-08-03 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 40, 'Louise', 'SERT', '1977-05-13', '282011304702159', 'MUTUELLE_GREASQUE', '2019-10-21 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 30, 'Jacques', 'GRES', '1980-05-29', '192011304702159', 'AZUR', '2020-10-26 00:00:00', 4, 4),
    (nextval('customer_id_seq'), 50, 'Henri', 'SALT', '1967-06-25', '172011304702159', 'MUTUELLE_DE_FRANCE', '2015-09-19 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 58, 'Bruno', 'DERT', '1962-05-15', '164011304702159', 'MUTUELLE_DU_NORD', '2005-11-12 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 35, 'Malik', 'LOIT', '1977-04-24', '187011304702159', 'MUTUELLE_PACA', '2010-05-09 00:00:00', 7, 7),
    (nextval('customer_id_seq'), 50, 'Sandra', 'FRUT', '1967-11-14', '272011304702159', 'ALPES_MUTUELLE', '2015-09-15 00:00:00', 10, 10);

CREATE SEQUENCE IF NOT EXISTS juridic_protection_id_seq;

insert into juridic_protection(
    juridic_protection_id,
    juridic_protection_name
)
values
    (nextval('juridic_protection_id_seq'), 'TRUSTEESHIP'),
    (nextval('juridic_protection_id_seq'), 'TUTORSHIP'),
    (nextval('juridic_protection_id_seq'), 'FAMILY_GUARDIANSHIP'),
    (nextval('juridic_protection_id_seq'), 'NOT_CONCERNED');

CREATE SEQUENCE IF NOT EXISTS juridic_protection_list_id_seq;

insert into juridic_protection_list(
    customer_id,
    juridic_protection_id
)
values
    (1 , 1),
    (2 , 2),
    (3 , 3),
    (4 , 4),
    (5 , 1),
    (6 , 2),
    (7 , 3),
    (8 , 4),
    (9 , 1),
    (10 , 2),
    (11 , 3),
    (12 , 4),
    (13 , 1),
    (14 , 2),
    (15 , 3),
    (16 , 4),
    (17 , 1),
    (18 , 2),
    (19 , 3),
    (20 , 4),
    (21 , 1),
    (22 , 2),
    (23 , 3),
    (24 , 4),
    (25 , 1),
    (26 , 2),
    (27 , 3),
    (28 , 4),
    (29 , 1),
    (30 , 2),
    (31 , 3),
    (32 , 4),
    (33 , 1),
    (34 , 2),
    (35 , 3),
    (36 , 4),
    (37 , 1),
    (38 , 2),
    (39 , 3),
    (40 , 4),
    (41 , 1),
    (42 , 2),
    (43 , 3),
    (44 , 4),
    (45 , 1),
    (46 , 2),
    (47 , 3),
    (48 , 4),
    (49 , 1),
    (50 , 2);

CREATE SEQUENCE IF NOT EXISTS establishment_id_seq;

insert into establishment(
    establishment_id,
    establishment_name
)
values
    (nextval('establishment_id_seq'), 'FOYER_HEB'),
    (nextval('establishment_id_seq'), 'FOYER_VIE'),
    (nextval('establishment_id_seq'), 'OTHER');

CREATE SEQUENCE IF NOT EXISTS establishment_list_id_seq;

insert into establishment_list(
    customer_id,
    establishment_id
)
values
    (1 , 1),
    (2 , 2),
    (3 , 3),
    (4 , 1),
    (5 , 2),
    (6 , 3),
    (7 , 1),
    (8 , 2),
    (9 , 3),
    (10 , 1),
    (11 , 2),
    (12 , 3),
    (13 , 1),
    (14 , 2),
    (15 , 3),
    (16 , 1),
    (17 , 2),
    (18 , 3),
    (19 , 1),
    (20 , 2),
    (21 , 3),
    (22 , 1),
    (23 , 2),
    (24 , 1),
    (25 , 2),
    (26 , 3),
    (27 , 1),
    (28 , 2),
    (29 , 3),
    (30 , 1),
    (31 , 2),
    (32 , 3),
    (33 , 1),
    (34 , 2),
    (35 , 3),
    (36 , 1),
    (37 , 2),
    (38 , 3),
    (39 , 1),
    (40 , 2),
    (41 , 3),
    (42 , 1),
    (43 , 2),
    (44 , 3),
    (45 , 1),
    (46 , 2),
    (47 , 3),
    (48 , 1),
    (49 , 2),
    (50 , 3);

CREATE SEQUENCE IF NOT EXISTS user_establishments_id_seq;

insert into user_establishments(
    user_establishment_id,
    establishment_id
)
values
    (1, 1),
    (2, 1),
    (3, 1),
    (4, 1),
    (5, 1),
    (6, 1),
    (7, 1),
    (8, 1),
    (9, 1),
    (10, 1);

CREATE SEQUENCE IF NOT EXISTS indirect_benefits_id_seq;

insert into indirect_benefits(
    indirect_benefits_id,
    indirect_benefit_name,
    indirect_benefit_kind
)
values
    (nextval('indirect_benefits_id_seq'), '3.1.1.1 Pilotage et direction', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.1.2 Gestion des ressources humaines, de la Gestion prévisionnelle des emplois et des competences et du dialogue social', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.2.1 Gestion budgétaire, financière et comptable', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.2.2 Gestion administrative', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.3.1 Communication (interne et externe), statistiques, rapport annuel et documents collectifs 2002-2', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.3.2 Gestion des données des personnes accueillies, système d’information, informatique, TIC, archivage informatique des données, GED', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.4.1 Démarche d’amélioration continue de la qualité', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.4.2 Analyse des pratiques, espaces ressource et soutien aux personnels', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.4.3 Prestations de supervision', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.5.1 Coopérations, conventions avec les acteurs spécialisés et du droit commun', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.5.2 Appui-Ressources et partenariats institutionnels', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.1.6 Transports liés à gérer, manager, coopérer', 'MANAGE_AND_COOPERATION_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.1.1 Locaux et autres ressources pour héberger', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.1.2 Locaux et autres ressources pour accueillir le jour', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.1.3 Locaux et autres ressources pour réaliser des prestations de soins, de maintien et de développement des capacités fonctionnelles', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.1.4 Locaux et autres ressources pour gérer, manager, coopérer', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.1.5 Hygiène, entretien, sécurité des locaux, espaces extérieurs', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.2.1 Fournir des repas', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.3.1 Entretenir le linge', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.4.1 Transports liés à accueillir (domicile-structure)', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.4.2 Transports liés aux prestations de soins, de maintien et de développement des capacités fonctionnelles', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.4.3 Transports liés à l’autonomie et la participation sociale', 'LOGISTIC_FUNCTION'),
    (nextval('indirect_benefits_id_seq'), '3.2.5 Transports des biens et matériels liés à la restauration et à l''entretien du linge', 'LOGISTIC_FUNCTION');

CREATE SEQUENCE IF NOT EXISTS indirect_benefits_list_id_seq;

insert into indirect_benefits_list(
    customer_id,
    indirect_benefits_id
)
values
    (1 , 1),
    (2 , 2),
    (3 , 3),
    (4 , 4),
    (5 , 5),
    (6 , 6),
    (7 , 7),
    (8 , 8),
    (9 , 9),
    (10 , 10),
    (11 , 11),
    (12 , 12),
    (13 , 13),
    (14 , 14),
    (15 , 15),
    (16 , 16),
    (17 , 17),
    (18 , 18),
    (19 , 19),
    (20 , 20),
    (21 , 21),
    (22 , 22),
    (23 , 23),
    (24 , 1),
    (25 , 2),
    (26 , 3),
    (27 , 4),
    (28 , 5),
    (29 , 6),
    (30 , 7),
    (31 , 8),
    (32 , 9),
    (33 , 10),
    (34 , 11),
    (35 , 12),
    (36 , 13),
    (37 , 14),
    (38 , 15),
    (39 , 16),
    (40 , 17),
    (41 , 18),
    (42 , 19),
    (43 , 20),
    (44 , 21),
    (45 , 22),
    (46 , 23),
    (47 , 1),
    (48 , 2),
    (49 , 3),
    (50 , 4);

CREATE SEQUENCE IF NOT EXISTS direct_benefits_id_seq;

insert into direct_benefits(
    direct_benefits_id,
    direct_benefit_name,
    direct_benefit_kind
)
values
    (nextval('direct_benefits_id_seq'), '2.1.1.1 Soins médicaux à visée préventive, curative et palliative', 'HEALTH_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.1.1.2 Soins techniques et de surveillance infirmiers ou délégués', 'HEALTH_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.1.1.3 Prestations des psychologues', 'HEALTH_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.1.1.4 Prestations des pharmaciens et préparateurs en pharmacie', 'HEALTH_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.1.2.1 Prestations des auxiliaires médicaux, des instructeurs en locomotion et avéjistes', 'HEALTH_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.2.1.1 Accompagnements pour les actes de la vie quotidienne', 'AUTONOMY_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.2.1.2 Accompagnements pour la communication et les relations avec autrui', 'AUTONOMY_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.2.1.3 Accompagnements pour prendre des décisions adaptées et pour la sécurité', 'AUTONOMY_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.1.1 Accompagnements à l’expression du projet personnalisé', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.1.2 Accompagnements à l’exercice des droits et libertés', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.2.1 Accompagnements pour vivre dans un logement', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.2.2 Accompagnements pour accomplir les activités domestiques', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.3.1 Accompagnements pour mener sa vie d’élève, d’étudiant ou d’apprenti', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.3.2 Accompagnements pour préparer sa vie professionnelle', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.3.3 Accompagnements pour mener sa vie professionnelle', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.3.4 Accompagnements pour réaliser des activités de jour spécialisées', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.3.5 Accompagnements de la vie familiale, de la parentalité, de la vie affective et sexuelle', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.3.6 Accompagnements pour l’exercice de mandats électoraux, la représentation des pairs et la pair aidance', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.4.1 Accompagnements du lien avec les proches et le voisinage', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.4.2 Accompagnements pour la participation aux activités sociales et de loisirs', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.4.3 Accompagnements pour le développement de l’autonomie pour les déplacements', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.5.1 Accompagnements pour l’ouverture des droits', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.5.2 Accompagnements pour l’autonomie de la personne dans la gestion des ressources', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.3.5.3 Informations, conseils et mise en œuvre des mesures de protection des adultes', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS'),
    (nextval('direct_benefits_id_seq'), '2.4.1 Prestation de coordination renforcée pour la cohérence du parcours', 'SOCIAL_PARTICIPATION_DIRECT_BENEFITS');

CREATE SEQUENCE IF NOT EXISTS direct_benefits_list_id_seq;

insert into direct_benefits_list(
    customer_id,
    direct_benefits_id
)
values
    (1 , 1),
    (2 , 2),
    (3 , 3),
    (4 , 4),
    (5 , 5),
    (6 , 6),
    (7 , 7),
    (8 , 8),
    (9 , 9),
    (10 , 10),
    (11 , 11),
    (12 , 12),
    (13 , 13),
    (14 , 14),
    (15 , 15),
    (16 , 16),
    (17 , 17),
    (18 , 18),
    (19 , 19),
    (20 , 20),
    (21 , 21),
    (22 , 22),
    (23 , 23),
    (24 , 24),
    (25 , 25),
    (26 , 1),
    (27 , 2),
    (28 , 3),
    (29 , 4),
    (30 , 5),
    (31 , 6),
    (32 , 7),
    (33 , 8),
    (34 , 9),
    (35 , 10),
    (36 , 11),
    (37 , 12),
    (38 , 13),
    (39 , 14),
    (40 , 15),
    (41 , 16),
    (42 , 17),
    (43 , 18),
    (44 , 19),
    (45 , 20),
    (46 , 21),
    (47 , 22),
    (48 , 23),
    (49 , 24),
    (50 , 25);

CREATE SEQUENCE IF NOT EXISTS needs_id_seq;

insert into needs(
    needs_id,
    need_name,
    need_kind
)
values
    (nextval('needs_id_seq'), '1.1.1.1 Besoins en matière de fonctions mentales, psychiques, cognitives et du système nerveux', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.1.1.2 Besoins en matière de fonctions sensorielles', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.1.1.3 Besoins en matière de douleur', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.1.1.4 Besoins relatifs à la voix, à la parole et à l’appareil bucco-dentaire', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.1.1.5 Besoins en matière de fonctions cardio-vasculaire, hématopoïétique, immunitaire et respiratoire', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.1.1.6 Besoins en matière de fonctions digestive, métabolique et endocrinienne', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.1.1.7 Besoins en matière de fonctions génito-urinaire et reproductive', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.1.1.8 Besoins en matière de fonctions locomotrices', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.1.1.9 Besoins relatifs à la peau et aux structures associées', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.1.1.10 Besoins pour entretenir et prendre soin de sa santé', 'HEALTH_NEEDS'),
    (nextval('needs_id_seq'), '1.2.1.1 Besoins en lien avec l’entretien personnel', 'AUTONOMY_NEEDS'),
    (nextval('needs_id_seq'), '1.2.1.2 Besoins en lien avec les relations et les interactions avec autrui', 'AUTONOMY_NEEDS'),
    (nextval('needs_id_seq'), '1.2.1.3 Besoins pour la mobilité', 'AUTONOMY_NEEDS'),
    (nextval('needs_id_seq'), '1.3.1.1 Besoins pour accéder aux droits et à la citoyenneté', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.2.1 Besoins pour vivre dans un logement', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.2.2 Besoins pour accomplir les activités domestiques', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.3.1 Besoins en lien avec la vie scolaire et étudiante', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.3.2 Besoins en lien avec le travail et l’emploi', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.3.3 Besoins transversaux en matière d’apprentissages', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.3.4 Besoins pour la vie familiale, la parentalité, la vie affective et sexuelle', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.3.5 Besoins pour apprendre à être pair-aidant', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.4.1 Besoins pour participer à la vie sociale', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.4.2 Besoins pour se déplacer avec un moyen de transport', 'SOCIAL_PARTICIPATION_NEEDS'),
    (nextval('needs_id_seq'), '1.3.5.1 Besoins en matière de ressources et d’autosuffisance économique', 'SOCIAL_PARTICIPATION_NEEDS');

CREATE SEQUENCE IF NOT EXISTS needs_list_id_seq;

insert into needs_list(
    customer_id,
    needs_id
)
values
    (1 , 1),
    (2 , 2),
    (3 , 3),
    (4 , 4),
    (5 , 5),
    (6 , 6),
    (7 , 7),
    (8 , 8),
    (9 , 9),
    (10 , 10),
    (11 , 11),
    (12 , 12),
    (13 , 13),
    (14 , 14),
    (15 , 15),
    (16 , 16),
    (17 , 17),
    (18 , 18),
    (19 , 19),
    (20 , 20),
    (21 , 21),
    (22 , 22),
    (23 , 23),
    (24 , 24),
    (25 , 1),
    (26 , 2),
    (27 , 3),
    (28 , 4),
    (29 , 5),
    (30 , 6),
    (31 , 7),
    (32 , 8),
    (33 , 9),
    (34 , 10),
    (35 , 11),
    (36 , 12),
    (37 , 13),
    (38 , 14),
    (39 , 15),
    (40 , 16),
    (41 , 17),
    (42 , 18),
    (43 , 19),
    (44 , 20),
    (45 , 21),
    (46 , 22),
    (47 , 23),
    (48 , 24),
    (49 , 1),
    (50 , 2);

CREATE SEQUENCE IF NOT EXISTS role_id_seq;

insert into role(
    role_id,
    role_name
)
values
    (nextval('role_id_seq'), 'ROLE_USER'),
    (nextval('role_id_seq'), 'ROLE_MODERATOR'),
    (nextval('role_id_seq'), 'ROLE_ADMIN');

CREATE SEQUENCE IF NOT EXISTS user_roles_id_seq;

insert into user_roles(
    user_roles_id,
    role_id
)
values
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 1),
    (5, 2),
    (6, 3),
    (7, 1),
    (8, 2),
    (9, 3),
    (10, 1);