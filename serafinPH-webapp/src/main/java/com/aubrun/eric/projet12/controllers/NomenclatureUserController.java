package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.dto.AddEstablishmentsDto;
import com.aubrun.eric.projet12.business.dto.AddRoleDto;
import com.aubrun.eric.projet12.business.dto.NomenclatureUserDto;
import com.aubrun.eric.projet12.business.service.NomenclatureUserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class NomenclatureUserController {

    private final NomenclatureUserService nomenclatureUserService;

    public NomenclatureUserController(NomenclatureUserService nomenclatureUserService) {
        this.nomenclatureUserService = nomenclatureUserService;
    }

    @GetMapping("/")
    public List<NomenclatureUserDto> getAllNomenclatureUsers() {
        return this.nomenclatureUserService.findAll();
    }

    @GetMapping("/{id}")
    public NomenclatureUserDto getNomenclatureUserById(@PathVariable(value = "id") int userId) {
        return this.nomenclatureUserService.findById(userId);
    }

    @PostMapping("/")
    public void createNomenclatureUser(@RequestBody NomenclatureUserDto nomenclatureUserDto) {
        nomenclatureUserService.save(nomenclatureUserDto);
    }

    @DeleteMapping("/{id}")
    public void deleteNomenclatureUser(@PathVariable("id") int userId) {
        nomenclatureUserService.delete(userId);
    }

    @PutMapping("/addReleaseDateUser/{id}")
    public void updateCustomer(@RequestBody NomenclatureUserDto nomenclatureUserDto, @PathVariable(value = "id") int nomenclatureUserId) {
        nomenclatureUserService.updateNomenclatureUser(nomenclatureUserId, nomenclatureUserDto.getReleaseDateUser());
    }

    @PutMapping("/addEstablishmentUser/{id}")
    public void addEstablishment(@RequestBody AddEstablishmentsDto addEstablishmentsDto, @PathVariable(value = "id") int nomenclatureUserId){
        nomenclatureUserService.addEstablishment(nomenclatureUserId, addEstablishmentsDto.getEstablishments());
    }

    @PutMapping("/addRole/{id}")
    public void addRole(@RequestBody AddRoleDto addRoleDto, @PathVariable(value = "id") int nomenclatureUserId){
        nomenclatureUserService.addRole(nomenclatureUserId, addRoleDto.getRoles());
    }

    @DeleteMapping("/delEstablishment/{id}")
    public void deleteByEstablishment(@PathVariable(value = "id") int nomenclatureUserId){
        nomenclatureUserService.deleteEstablishment(nomenclatureUserId);
    }

    @DeleteMapping("/delRole/{id}")
    public void deleteRole(@PathVariable(value = "id") int nomenclatureUserId){
        nomenclatureUserService.deleteRole(nomenclatureUserId);
    }
}
