package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.dto.IndirectBenefitsDto;
import com.aubrun.eric.projet12.business.service.IndirectBenefitsService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/indirectBenefits")
public class IndirectBenefitsController {

    private final IndirectBenefitsService indirectBenefitsService;

    public IndirectBenefitsController(IndirectBenefitsService indirectBenefitsService) {
        this.indirectBenefitsService = indirectBenefitsService;
    }

    @GetMapping("/")
    public List<IndirectBenefitsDto> getAllIndirectBenefits() {
        return this.indirectBenefitsService.findAll();
    }

    @GetMapping("/{id}")
    public IndirectBenefitsDto getIndirectBenefitsById(@PathVariable(value = "id") int indirectBenefitsId) {
        return this.indirectBenefitsService.findById(indirectBenefitsId);
    }

    @PostMapping("/")
    public void createIndirectBenefits(Principal principal, @RequestBody IndirectBenefitsDto indirectBenefitsDto) {
        indirectBenefitsService.save(principal.getName(), indirectBenefitsDto);
    }

    @PutMapping("/{id}")
    public void updateIndirectBenefits(@RequestBody IndirectBenefitsDto indirectBenefitsDto, @PathVariable(value = "id") int indirectBenefitsDtoId) {
        indirectBenefitsService.update(indirectBenefitsDto, indirectBenefitsDtoId);
    }

    @DeleteMapping("/{id}")
    public void deleteNeeds(@PathVariable(value = "id") int indirectBenefitsId) {
        indirectBenefitsService.deleteById(indirectBenefitsId);
    }
}
