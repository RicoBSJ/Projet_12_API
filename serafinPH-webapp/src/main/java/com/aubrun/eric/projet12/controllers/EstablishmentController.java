package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.dto.EstablishmentDto;
import com.aubrun.eric.projet12.business.service.EstablishmentService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/establishments")
public class EstablishmentController {

    private final EstablishmentService establishmentService;

    public EstablishmentController(EstablishmentService establishmentService) {
        this.establishmentService = establishmentService;
    }

    @GetMapping("/")
    public List<EstablishmentDto> getAllEstablishments() {
        return this.establishmentService.findAll();
    }

    @GetMapping("{id}")
    public EstablishmentDto getEstablishmentById(@PathVariable(value = "id") int establishmentId) {
        return this.establishmentService.findById(establishmentId);
    }

    @PostMapping("/")
    public void createEstablishment(Principal principal, @RequestBody EstablishmentDto establishmentDto) {
        establishmentService.save(principal.getName(), establishmentDto);
    }

    @PutMapping("/{id}")
    public void updateEstablishment(@RequestBody EstablishmentDto establishmentDto, @PathVariable(value = "id") int establishmentDtoId) {
        establishmentService.update(establishmentDto, establishmentDtoId);
    }

    @DeleteMapping("/{id}")
    public void deleteEstablishment(@PathVariable(value = "id") int establishmentId) {
        establishmentService.deleteById(establishmentId);
    }
}
