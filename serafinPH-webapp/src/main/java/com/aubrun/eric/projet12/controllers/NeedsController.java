package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.dto.NeedsDto;
import com.aubrun.eric.projet12.business.service.NeedsService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/needs")
public class NeedsController {

    private final NeedsService needsService;

    public NeedsController(NeedsService needsService) {
        this.needsService = needsService;
    }

    @GetMapping("/")
    public List<NeedsDto> getAllNeeds() {
        return this.needsService.findAll();
    }

    @GetMapping("/{id}")
    public NeedsDto getNeedsById(@PathVariable(value = "id") int needsId) {
        return this.needsService.findById(needsId);
    }

    @PostMapping("/")
    public void createNeeds(Principal principal, @RequestBody NeedsDto needsDto) {
        needsService.save(principal.getName(), needsDto);
    }

    @PutMapping("/{id}")
    public void updateNeeds(@RequestBody NeedsDto needsDto, @PathVariable(value = "id") int needsDtoId) {
        needsService.update(needsDto, needsDtoId);
    }

    @DeleteMapping("/{id}")
    public void deleteNeeds(@PathVariable(value = "id") int needsDtoId) {
        needsService.deleteById(needsDtoId);
    }
}