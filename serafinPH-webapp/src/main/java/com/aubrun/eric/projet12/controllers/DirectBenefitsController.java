package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.dto.DirectBenefitsDto;
import com.aubrun.eric.projet12.business.service.DirectBenefitsService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/directBenefits")
public class DirectBenefitsController {

    private final DirectBenefitsService directBenefitsService;

    public DirectBenefitsController(DirectBenefitsService directBenefitsService) {
        this.directBenefitsService = directBenefitsService;
    }

    @GetMapping("/")
    public List<DirectBenefitsDto> getAllDirectBenefits() {
        return this.directBenefitsService.findAll();
    }

    @GetMapping("/{id}")
    public DirectBenefitsDto getDirectBenefitsById(@PathVariable(value = "id") int directBenefitsId) {
        return this.directBenefitsService.findById(directBenefitsId);
    }

    @PostMapping("/")
    public void createDirectBenefits(Principal principal, @RequestBody DirectBenefitsDto directBenefitsDto){
        directBenefitsService.save(principal.getName(), directBenefitsDto);
    }

    @PutMapping("/{id}")
    public void updateDirectBenefits(@RequestBody DirectBenefitsDto directBenefitsDto, @PathVariable(value = "id") int directBenefitsId){
        directBenefitsService.update(directBenefitsDto, directBenefitsId);
    }

    @DeleteMapping("/{id}")
    public void deleteEstablishment(@PathVariable(value = "id") int directBenefitsId) {
        directBenefitsService.deleteById(directBenefitsId);
    }
}
